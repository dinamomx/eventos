/**
 * Schema.org Structured Data
 * @type {Object}
 * @link https://jsonld.com/
 * @link https://searchengineland.com/schema-markup-structured-data-seo-opportunities-site-type-231077
 * @link https://docs.google.com/spreadsheets/d/1Ed6RmI01rx4UdW40ciWgz2oS_Kx37_-sPi7sba_jC3w/edit#gid=0
 */

const address = {
  '@type': 'PostalAddress',
  addressLocality: 'Coyoacán',
  addressRegion: 'CDMX',
  postalCode: '04450',
  streetAddress: 'Erasmo Castellanos Quinto 127',
}
const sameAs = [
  'https://www.facebook.com/JaguarCarsMx/',
  'https://twitter.com/JaguarCarsMx/',
  'https://www.linkedin.com/company/JaguarCarsMx',
  'https://www.instagram.com/JaguarCarsMx/',
]
const description = 'Expertos en estrategia digital, diseño web, social media, fotografía...'
module.exports = {
  '@context': 'http://schema.org',
  '@graph': [
    // LocalBusiness
    {
      '@type': 'LocalBusiness',
      address,
      name: 'Dinamo Agencia de Comunicación',
      description,
      openingHours: [
        'Mo-Fr 9:00-19:00',
      ],
      telephone: '55 4140 8714',
      url: 'http://eventosjaguarlandrover.com',
      logo: {
        '@context': 'http://schema.org',
        '@type': 'ImageObject',
        author: 'Dinamo',
        contentLocation: 'Ciudad de Mexico, Mexico',
        contentUrl: 'https://dinamo.mx/logo-dinamo.jpg',
        description: 'Agencia de Comunicación.',
        name: 'Dinamo',
      },
      image: 'http://eventosjaguarlandrover.com/assets/jaguar-landrover.png',
      geo: {
        '@type': 'GeoCoordinates',
        latitude: '19.4316886',
        longitude: '-99.1925079',
      },
      hasMap: 'https://www.google.com.mx/maps/place/Jaguar+y+Land+Rover+Masaryk/@19.4320568,-99.1924747,17.18z/data=!4m12!1m6!3m5!1s0x85d202000ea92795:0xf87b9d8aee315ebe!2sJaguar+y+Land+Rover+Masaryk!8m2!3d19.431687!4d-99.190299!3m4!1s0x85d202000ea92795:0xf87b9d8aee315ebe!8m2!3d19.431687!4d-99.190299?hl=en',
      sameAs,
    },
    // Organization
    {
      '@type': 'Organization',
      name: 'Jaguar Land Rover Agencia de autos',
      legalName: 'Jaguar Land Rover',
      url: 'https://eventosjaguarlandrover.com',
      logo: 'https://eventosjaguarlandrover.com/assets/jaguar-landrover.png',
      foundingDate: '2009',
      address,
      sameAs,
    },
    {
      '@type': 'WebSite',
      url: 'http://www.eventosjaguarlandrover.com/',
      name: 'Jaguar Land Rover Agencia de autos',
      author: {
        '@type': 'Organization',
        name: 'Jaguar Land Rover Agencia de autos',
      },
      description,
      publisher: 'Jaguar Land Rover Agencia de autos',
    },
  ],
}
