/**
 * Head de vue-meta
 * @type {Object}
 * @url https://github.com/declandewet/vue-meta
 */
const icons = require('./icons')
const og = require('./og-data')
const schema = require('./schema')

/**
 * Propiedades meta de html
 * @type {Array} meta
 */
const meta = [
  { charset: 'utf-8' },
  { name: 'keywords', content: 'Jaguar Land Rover' },
  { name: 'viewport', content: 'width=device-width, initial-scale=1, minimum-scale=1' },
  { name: 'apple-mobile-web-app-title', content: 'Dinamo' },
  { name: 'application-name', content: 'Dinamo' },
  { name: 'msapplication-TileColor', content: '#211f35' },
  { name: 'google-site-verification', content: 'Jed-HEqd5HFV8vCgFCZOGSav13zUvsG73r4am7daIHM' },
  { name: 'msapplication-TileImage', content: '/mstile-144x144.png?v=QEMEyKyxR8' },
  { name: 'theme-color', content: '#211f35' },
  {
    hid: 'description', vmid: 'description', name: 'description', content: 'Eventos Jaguar Land Rover',
  },
]

meta.splice(2, 0, ...og)

const link = [
  {
    href: 'https://fonts.googleapis.com/css?family=Quicksand|Rubik:400,700',
    rel: 'stylesheet',
    // Truco para tener css asíncrono
    media: 'none',
    onload: "if(media!='all')media='all'",
  },
]

link.splice(2, 0, ...icons)


module.exports = {
  titleTemplate: '%s - Jaguar Land Rover',
  link,
  meta,
  script: [
    { innerHTML: JSON.stringify(schema), type: 'application/ld+json' },
  ],
  noscript: [
    { innerHTML: 'Para tener una experiencia óptima, habilita JavaScript en tu navegador.' },
  ],
  __dangerouslyDisableSanitizers: ['script'],
}
