module.exports = [
  { rel: 'icon', type: 'image/x-icon', href: '/favicon.ico' },
  { rel: 'apple-touch-icon', sizes: '180x180', href: '/apple-touch-icon.png?v=QEMEyKyxR8' },
  {
    rel: 'icon', type: 'image/png', sizes: '32x32', href: '/favicon-32x32.png?v=QEMEyKyxR8',
  },
  {
    rel: 'icon', type: 'image/png', sizes: '16x16', href: '/favicon-16x16.png?v=QEMEyKyxR8',
  },
  { rel: 'manifest', href: '/manifest.json?v=QEMEyKyxR8' },
  { rel: 'mask-icon', href: '/safari-pinned-tab.svg?v=QEMEyKyxR8', color: '#f83f28' },
  { rel: 'shortcut icon', href: '/favicon.ico?v=QEMEyKyxR8' },
]
