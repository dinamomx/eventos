module.exports = {
  /**
   * Application configuration section
   * http://pm2.keymetrics.io/docs/usage/application-declaration/
   * BUG: no funcionará debido a una incompatibilidad entre
   * nuxtent y express
   */
  apps: [

    // First application
    {
      name: 'dinamo.mx',
      script: 'build/main.js',
      env: {
        PORT: '3010',
      },
      env_production: {
        NODE_ENV: 'production',
        DEBUG: '',
      },
    },
  ],
  /**
   * Deployment section
   * http://pm2.keymetrics.io/docs/usage/deployment/
   */
  deploy: {
    production: {
      user: 'node',
      host: '72.10.35.234',
      ref: 'origin/master',
      repo: 'git@bitbucket.org:dinamomx/dinamo-nuxt.git',
      path: '/home/node/www/plantilla.dinamo.mx',
      'pre-deploy': 'git pull && git submodule update',
      'post-deploy': 'yarn install --prod && NODE_ENV=production yarn build && pm2 startOrReload ecosystem.config.js --env production',
    },
  },
}
